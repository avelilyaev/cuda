#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//��������� �������-----------------------------------------------------------------------------------------
void PrintInfoAboutCurrentVideoCard();
void ReadMatrixFromFile();
void TransposeMatrix();
void PrintMatrix();

//���������� ����������-------------------------------------------------------------------------------------
//�������� �������
int *Matrix;

//����������������� �������
int *ResultMatrix;

//����������: 
//����� ����� � ������� ����������� � ������ ������ �������� �����

//���������� �����
int countStrings;

//���������� ��������
int countColumns;

//����------------------------------------------------------------------------------------------------------
__global__ void KernelFunction(int StringsCount, int ColumnCount, int *Matrix, int *ResultMatrix)
{
	int i = threadIdx.x;
	int index = i * StringsCount;
	for (int j = 0; j < StringsCount; j++)
	{
		ResultMatrix[index] = Matrix[j * ColumnCount + i];
		index++;
	}
}

int main()
{
	//������� ���������� � ����� ����������
	PrintInfoAboutCurrentVideoCard();

	//�������� ���� ���������� ��� �������������
	cudaSetDevice(0);

	//��������� ������� �� ����� "matrix.txt"
	ReadMatrixFromFile();

	//������������� �������
	//TransposeMatrix();

	dim3 gridDim = dim3(countColumns);
	dim3 blockDim = dim3(countColumns);

	int *deviceMatrix, *deviceResultMatrix;
	cudaMalloc(&deviceMatrix, 4 * countStrings * countColumns);
	cudaMalloc(&deviceResultMatrix, 4 * countStrings * countColumns);

	////�������� ������ "Matrix" (�� CPU) � ������ "deviceMatrix" (� GPU)
	cudaMemcpy(deviceMatrix, Matrix, 4 * countStrings * countColumns, cudaMemcpyHostToDevice);

	KernelFunction <<< gridDim, blockDim >>>(countStrings, countColumns, deviceMatrix, deviceResultMatrix);

	cudaMemcpy(ResultMatrix, deviceResultMatrix, 4 * countStrings * countColumns, cudaMemcpyDeviceToHost);

	//������� �������
	PrintMatrix();

	getchar();
	return 0;
}

//�������, ��������� ���������� � ������� ����������
void PrintInfoAboutCurrentVideoCard()
{
	cudaDeviceProp deviceProperties;
	cudaGetDeviceProperties(&deviceProperties, 0);
	printf("Device's Name: %s \n", deviceProperties.name);
	printf("Total Global Memory: %i Mb \n", deviceProperties.totalGlobalMem / 1024 / 1024);
	printf("-----------------------------------------------------------------------\n");
}

//������� ������ ������� �� �����
void ReadMatrixFromFile()
{
	FILE * file = fopen("matrix.txt", "r");
	int i = 0;

	//��������� ���������� �����
	fscanf(file, "%i", &countStrings);

	//��������� ���������� ��������
	fscanf(file, "%i", &countColumns);

	//�������� ������ ��� �������� �������:
	Matrix = (int *)malloc(countColumns * countStrings * sizeof(int));

	//�������� ������ ��� ���������������� �������:
	ResultMatrix = (int *)malloc(countColumns * countStrings * sizeof(int));

	i = 0;
	while (!feof(file))
	{
		int j = 0;
		char currentString[1000];
		fgets(currentString, sizeof(currentString), file);

		char * p = strtok(currentString, " '\n'");
		if (p != NULL)
		{
			Matrix[i * countColumns + j] = atoi(p);
			j++;
			while (p != NULL)
			{
				p = strtok(NULL, " '\n'");
				if (p != NULL)
				{
					Matrix[i * countColumns + j] = atoi(p);
					j++;
				}
			}
			i++;
		}
	}
	fclose(file);
}

//������� ������ ������� � ���� "resultMarix.txt"
void PrintMatrix()
{
	FILE * file = fopen("resultMarix.txt", "w");
	for (int i = 0; i < countColumns * countStrings; i++)
	{
		fprintf(file, "%i ", ResultMatrix[i]);
		if ((i + 1) % countStrings == 0)
		{
			fprintf(file, "\n");
		}
	}
	fclose(file);
}

//�������, ��������������� �������
void TransposeMatrix()
{
	int index = 0;
	for (int i = 0; i < countColumns; i++)
	{
		for (int j = 0; j < countStrings; j++)
		{
			ResultMatrix[index] = Matrix[j * countColumns + i];
			index++;
		}
	}
}